# This file will demonstrate continue Statements

# while True:
#     print('Who are you?')
#     name = input()
#     if name != 'Joe':
#         continue
#     print('Hello, Joe. What is the password? (Is is a fish)')
#     password = input()
#     if password == 'swordfish':
# #         break
# # print('Access granted!')

# while True:
#     print('Which band is better, Tool or Pennywise?')
#     band = input()
#     if band != 'Tool':
#         continue
#     print('Name an album by them,')
#     album = input()
#     if album == 'Lateralus':
#         break
# print('Tool kicks ass!')  

# while True:
#     print('Who are you?')
#     person = input()
#     if person != 'Bob':
#         continue
#     print('Hello Bob, what is the password?')
#     password = input()
#     if password == 'AlexC913':
#         break
# print('Access Granted!')

while True:
    print('Who are you? ')
    human = input()
    if human != 'Greg':
        continue
    print('Hi there Greg, whats the password?')
    pw = input()
    if pw == 'Greg123':
        break
print('Access granted')
        