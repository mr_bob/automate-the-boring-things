# This file is for "while loops"

# spam = 0
# if spam < 5:
#     print('Hello world!')
#     spam = spam + 1

# spam = 0
# while spam < 5:
#     print('Hello world!')
#     spam = spam + 1 

food = 0
while food < 5:
    print('\nThis food is great!')  
    food = food + 1  

       