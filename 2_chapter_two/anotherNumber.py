# Lets guess another number

import random

lotteryNumber = random.randint(1, 10)
print('Guess the winning lottery number!')

for guesses in range(1, 6):
    print('Take a guess!')
    number = int(input())

    if number < lotteryNumber:
        print('Your guess is too low.')
    elif number > lotteryNumber:
        print('Your guess is too high.')
    else:
        break

if guesses == lotteryNumber:
    print('Nice work! You guessed the winning number in ' + str(number) + ' tries.')
else:
    print('Sorry, the winning number was ' + str(lotteryNumber) + '.')            
