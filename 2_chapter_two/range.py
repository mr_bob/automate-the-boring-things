# This file is for some range() learnings

for i in range(12, 16):
    print(i)

print('\n')    

for i in range(0, 10, 2):
    print(i)

print('\n')

for i in range(0, 10, 5):
    print(i)

print('\n')

for i in range(5, -1, -1):
    print(i)