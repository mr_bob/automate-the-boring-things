#for Loops and the range() Function

# print('My name is')
# for i in range(2):
#     print('Jimmy Two Time ('+str(i)+')')



# print('Ive been there:')
# for x in range(3):
#     print('this many times ('str+(x)+')')


# Rewriting using a while loop

print('My name is')
i = 0
while i < 5:
    print('Jimmy Five Times (' + str(i) + ')')
    i = i + 1