#An Annoying while Loop and Break Statements


# name = ''
# while name != 'your name':
#     print('Please type your name.')
#     name = input()
# print('Thank you!')   

# while True:
#     print('Please type your name.')
#     name = input()
#     if name == 'your name':
#         break
# print('Thank you!')

while True:
    print('Is Tool a good band?')
    band = input()
    if band == 'yes':
        break
print('I know right!')    

