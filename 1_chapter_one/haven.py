# Practicing more coding for Haven

print('Hi there! Whats your name?')
your_name = input()

print('\nHello ' + your_name.capitalize() + ', its nice to meet you!')
print('\nWhats your favorite snack?')
fav_snack = input()
print('\nWhoa!!! ' + fav_snack.title() + ' sounds kinda gross but whatever. Its your body, not mine.')
print('\nAnyways, name one of your puppies name.')
puppy_name = input()

print('\nI hear ' + puppy_name.capitalize() + ' loves you a lot cause you give them human food!')
print('\nName one place you would want to visit.')

place_visit = input()
print('\nYou know, Ive heard ' + place_visit.capitalize() + ' has a lot of cool opportunities for you.')
print('\nAs a life goal and within ' + str(int(5)) + ' years, you should make ' + place_visit.capitalize() + ' a reality!')