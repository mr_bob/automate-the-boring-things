# More function() practice

def hello(name):
    print('Hello, ' + name)

hello('Alice')
hello('Bob')
print(name) # This causes NameError because 'name' doesnt exist

# def girl(name):
#     print('Hi there ' + name)

# girl("Alex")

def sayHello(name):
    print('Hello, ' + name)
sayHello('Al')    


# def testing(test):
#     print('This is only a test ' + test)

# testing('fool!')    