import random
print('\n')
print('You deserve a compliment!')
print('\n')

def compliment(receive_comp):
    if receive_comp == 1:
        return 'You'
    elif receive_comp == 2:
        return 'YOU'    
print(compliment(random.randint(1, 2)))

def nice(awe):
    if awe == 1:
        return 'ARE'
    elif awe == 2:
        return 'are'
    
print(nice(random.randint(1, 2)))

def wow(damn):
    if damn == 1:
        return 'BEAUTIFUL!' 
    elif damn == 2:
        return 'AWESOME!'                 
    elif damn == 3:
        return 'SILLY!!' 

print(wow(random.randint(1, 3)))
