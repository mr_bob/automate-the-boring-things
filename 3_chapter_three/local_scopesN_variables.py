# Local Scopes Cannot Use Variables in Other Local Scopes

def spam():
    eggs = 99
    bacon()
    print(eggs)

def bacon():
    ham = 101
    eggs = 1
    print(eggs)

spam()
print('\n')

# Global Variables Can Be Read from a Local Scope

def spam():
    print(eggs)
eggs = 42
spam()
print(eggs)    

print('\n')

#Local and Global Variables with the Same Name

def spam():
    eggs = 'spam local'
    print(eggs) # prints 'spam local

def bacon():
    eggs = 'bacon local'
    print(eggs) # prints 'bacon local'
    spam()
    print(eggs) # prints 'bacon local'
print('\n')

eggs = 'global'
bacon()
print(eggs) # prints 'global'



