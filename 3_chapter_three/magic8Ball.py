import random

def getAnswer(answerNumber):
    if answerNumber == 1:
        return 'It is certain'
    elif answerNumber == 2:
        return 'It is decidedly so'
    elif answerNumber == 3:
        return 'Yes'
    elif answerNumber == 4:
        return 'Reply hazy, try again'
    elif answerNumber == 5:
        return 'Ask again later'
    elif answerNumber == 6:
        return 'Concentrate and ask again'
    elif answerNumber == 7:
        return 'My reply is no'
    elif answerNumber == 8:
        return 'Outlook not so good'
    elif answerNumber == 9:
        return 'Very doubful'

# r = random.randint(1, 9) 
# fortune = getAnswer(r)
# print(fortune)       

print(getAnswer(random.randint(1, 9)))

print('\n')

def yeah(right):
    if right == 1:
        return 'One'
    elif right == 2:
        return 'Two'
    elif right == 3:
        return 'Three'
    elif right == 4:
        return 'Four'
    elif right == 5:
        return 'Five'
    elif right == 6:
        return 'Six'
    elif right == 7:
        return 'Seven'
    elif right == 8:
        return 'Eight'
    elif right == 9:
        return 'Nine'
    elif right == 10:
        return 'Ten'
print(yeah(random.randint(1, 10)))

# r = random.randint(1, 10)
# thing = yeah(r)
# print(thing)
print('\n')

def hats(trucker_hats):
    if trucker_hats == 1:
        return 'Trucker Hats'
    elif trucker_hats == 2:
        return 'Flexfit Hats'
    elif trucker_hats == 3:
        return 'Fitted Hats'

print(hats(random.randint(1, 3)))
print('\n')

def bands(music):
    if music == 1:
        return 'Tool'
    elif music == 2:
        return 'Pennywise'
    elif music == 3:
        return 'NOFX'
    elif music == 4:
        return 'Metallica' 
print(bands(random.randint(1, 4)))    

print('\n')
def guitar(jamming):
    if jamming == 1:
        return 'Ibanez'
    elif jamming == 2:
        return 'Fender'
    elif jamming == 3:
        return 'Jackson' 
print(guitar(random.randint(1, 3)))                   














































