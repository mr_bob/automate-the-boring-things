
# def hello_func(greeting, name = 'You'):
#     return '{}, {} Function.'. format(greeting, name)

# print(hello_func('Hi'))


# DRY - Dont Repeat Yourself

# def student_info(*args, **kwargs):
#     print(args)
#     print(kwargs)

# student_info('Math', 'Art', name = 'John', age=22)


def  test_var_args(f_arg, *argv):
    print('first normal arg:', f_arg)
    for arg in argv:
        print('another arg through *argv:', arg)

test_var_args('yasoob', 'python', 'eggs', 'test')

def greet_me(**kwargs):
    for key, value in kwargs.items():
        print("{0} = {1}".format(key, value))

>>> greet_me(name="yasoob")
name = yasoob