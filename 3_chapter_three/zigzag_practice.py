

import time, sys
indent = 0
indent_increasing = True

on_off = True
# try:
count = 0
while on_off:
    print(' ' * indent, end='')
    print('press ctl c to end')
    time.sleep(0.1)

    if indent_increasing:
        indent = indent + 1
        if indent == 20:
            indent_increasing = False
            count += 1

    else:
        indent = indent - 1
        if indent == 0:
            indent_increasing = True
    if count > 3:
        on_off = False                
# # except KeyboardInterrupt:
#     sys.exit()                        