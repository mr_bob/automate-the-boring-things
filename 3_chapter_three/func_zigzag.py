

import time, sys
indent = 0
indent_increase = True

on_off = True

count = 0
while on_off:
    print(' ' * indent, end='')
    print('Zigzagging away!')
    time.sleep(0.1)

    if indent_increase:
        indent = indent + 1
        if indent == 20:
            indent_increase = False
            count += 1

    else:
        indent = indent - 1
        if indent == 0:
            indent_increase = True
    if count > 2:
        on_off = False                

