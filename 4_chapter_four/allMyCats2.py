# catNames = []

# while True:
#     print('Enter the name of cat ' + str(len(catNames) + 1) +
#         '(Or enter nothing to stop.):')
#     name = input()
#     if name == '':
#         break
#     catNames = catNames + [name] # list concatenation
# print('The cat names are:')
# for name in catNames:
#     print(' ' + name)    


guitars = []

while True:
    print('Enter the type of guitar you have ' + str(len(guitars) + 1) +
    '(Or hit the enter button to stop.')
    
    name = input()
    if name == '':
        break
    guitars = guitars + [name] # list concatenation
    print('The guitar names are: ')
    for name in guitars:
        print(' ' + name.title())
